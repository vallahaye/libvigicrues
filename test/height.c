/*
 * Copyright (c) 2017 Valentin Lahaye
 *
 * This file is part of a free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 3 of the License, or any later version.
 *
 * This program is distributed in the hope that it will be useful, but 
 * WITHOUT ANY WARRANTY; without even the implied warranty of 
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public License
 * along with this program. If not, see <http://www.gnu.org/licenses/lgpl-3.0.html>
 */

#include <stdlib.h>
#include <vigicrues/test.h>

vigicrues_status_t vigicrues_test_setup(vigicrues_test_t *p_test)
{
  vigicrues_status_t status = VIGICRUES_FAILURE;
  
  vigicrues_t *p_data = (vigicrues_t *)malloc(sizeof(vigicrues_t));
  if (p_data) {
    #ifdef __clang__
    #pragma clang diagnostic push
    #pragma clang diagnostic ignored "-Wmissing-field-initializers"
    #endif
    struct tm date = { 0 };
    #ifdef __clang__
    #pragma clang diagnostic pop
    #endif
    date.tm_year = 117; // 2017
    date.tm_mon = 5; // June
    date.tm_mday = 24;
    date.tm_hour = 9;
    date.tm_isdst = -1;
    
    p_data->type = VIGICRUES_HEIGHT;
    p_data->value = 0.8F;
    p_data->timestamp = mktime(&date); // 2017-06-24 09:00:00
    
    p_test->ent = VIGICRUES_IGNORE_PARAM;
    p_test->id = "J260301501";
    p_test->type = VIGICRUES_HEIGHT;
    p_test->range = 1;
    p_test->param = p_data;
    
    status = VIGICRUES_SUCCESS;
  }
  
  return status;
}

vigicrues_status_t vigicrues_test_teardown(vigicrues_test_t *p_test)
{
  free(p_test->param);
  
  return VIGICRUES_SUCCESS;
}

vigicrues_status_t vigicrues_test_func(const vigicrues_t *p_data, void *param)
{
  vigicrues_t *p_exp_data = (vigicrues_t *)param;
  
  return (p_data->type == p_exp_data->type &&
          p_data->value == p_exp_data->value &&
          p_data->timestamp == p_exp_data->timestamp)
    ? VIGICRUES_SUCCESS : VIGICRUES_FAILURE;
}

vigicrues_status_t vigicrues_test_validate(vigicrues_status_t status,
                                           vigicrues_error_t error)
{
  return status;
}
