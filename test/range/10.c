/*
 * Copyright (c) 2017 Valentin Lahaye
 *
 * This file is part of a free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 3 of the License, or any later version.
 *
 * This program is distributed in the hope that it will be useful, but 
 * WITHOUT ANY WARRANTY; without even the implied warranty of 
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public License
 * along with this program. If not, see <http://www.gnu.org/licenses/lgpl-3.0.html>
 */

#include <stdlib.h>
#include <time.h>
#include <vigicrues/test.h>

#define VIGICRUES_TEST_RANGE 10

struct vigicrues_param_s {
  unsigned i;
  vigicrues_t data[VIGICRUES_TEST_RANGE];
};

static inline void vigicrues_test_setup_data(vigicrues_t *p_data, float value,
                                             int year, int mon, int mday,
                                             int hour, int min);

vigicrues_status_t vigicrues_test_setup(vigicrues_test_t *p_test)
{
  vigicrues_status_t status = VIGICRUES_FAILURE;
  
  struct vigicrues_param_s *p_param = (struct vigicrues_param_s *)malloc(sizeof(struct vigicrues_param_s));
  if (p_param) {
    p_param->i = 0;
    
    vigicrues_test_setup_data(&(p_param->data[9]), 6.78F, 2017, 6, 19, 0, 0);
    vigicrues_test_setup_data(&(p_param->data[8]), 7.36F, 2017, 6, 19, 1, 0);
    vigicrues_test_setup_data(&(p_param->data[7]), 7.49F, 2017, 6, 19, 2, 0);
    vigicrues_test_setup_data(&(p_param->data[6]), 7.22F, 2017, 6, 19, 3, 0);
    vigicrues_test_setup_data(&(p_param->data[5]), 6.54F, 2017, 6, 19, 4, 0);
    vigicrues_test_setup_data(&(p_param->data[4]), 5.61F, 2017, 6, 19, 5, 0);
    vigicrues_test_setup_data(&(p_param->data[3]), 4.52F, 2017, 6, 19, 6, 0);
    vigicrues_test_setup_data(&(p_param->data[2]), 4.44F, 2017, 6, 19, 7, 0);
    vigicrues_test_setup_data(&(p_param->data[1]), 4.43F, 2017, 6, 19, 8, 0);
    vigicrues_test_setup_data(&(p_param->data[0]), 4.43F, 2017, 6, 19, 9, 0);
    
    p_test->ent = VIGICRUES_IGNORE_PARAM;
    p_test->id = "J262301002";
    p_test->type = VIGICRUES_HEIGHT;
    p_test->range = VIGICRUES_TEST_RANGE;
    p_test->param = p_param;
    
    status = VIGICRUES_SUCCESS;
  }
  
  return status;
}

vigicrues_status_t vigicrues_test_teardown(vigicrues_test_t *p_test)
{
  free(p_test->param);
  
  return VIGICRUES_SUCCESS;
}

vigicrues_status_t vigicrues_test_func(const vigicrues_t *p_data, void *param)
{
  struct vigicrues_param_s *p_param = (struct vigicrues_param_s *)param;
  vigicrues_t *p_exp_data = &(p_param->data[p_param->i++]);
  
  return (p_data->type == p_exp_data->type &&
          p_data->value == p_exp_data->value &&
          p_data->timestamp == p_exp_data->timestamp)
    ? VIGICRUES_SUCCESS : VIGICRUES_FAILURE;
}

vigicrues_status_t vigicrues_test_validate(vigicrues_status_t status,
                                           vigicrues_error_t error)
{
  return status;
}

static inline void vigicrues_test_setup_data(vigicrues_t *p_data, float value,
                                             int year, int mon, int mday,
                                             int hour, int min)
{
  #ifdef __clang__
  #pragma clang diagnostic push
  #pragma clang diagnostic ignored "-Wmissing-field-initializers"
  #endif
  struct tm date = { 0 };
  #ifdef __clang__
  #pragma clang diagnostic pop
  #endif
  date.tm_year = year - 1900;
  date.tm_mon = mon - 1;
  date.tm_mday = mday;
  date.tm_hour = hour;
  date.tm_min = min;
  date.tm_isdst = -1;
  
  p_data->type = VIGICRUES_HEIGHT;
  p_data->value = value;
  p_data->timestamp = mktime(&date);
}
