/*
 * Copyright (c) 2017 Valentin Lahaye
 *
 * This file is part of a free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 3 of the License, or any later version.
 *
 * This program is distributed in the hope that it will be useful, but 
 * WITHOUT ANY WARRANTY; without even the implied warranty of 
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public License
 * along with this program. If not, see <http://www.gnu.org/licenses/lgpl-3.0.html>
 */

#include <stdlib.h>
#include <stdio.h>
#include <vigicrues.h>
#include <vigicrues/test.h>

static inline void vigicrues_test_print_status(const char *step,
                                               vigicrues_status_t status);

int main(int argc, char *argv[]) {
  vigicrues_test_t test;
  
  vigicrues_status_t status = vigicrues_test_setup(&test);
  vigicrues_test_print_status("Setup", status);
  
  if (status == VIGICRUES_SUCCESS) {
    vigicrues_error_t error;
    status = vigicrues_get(test.ent, test.id, test.type, test.range,
                           &vigicrues_test_func, test.param,
                           VIGICRUES_IGNORE_PARAM, &error);
    vigicrues_test_print_status("Run", status);
    
    status = vigicrues_test_validate(status, error);
    vigicrues_test_print_status("Validation", status);
    
    vigicrues_status_t memento = status;
    status = vigicrues_test_teardown(&test);
    vigicrues_test_print_status("Teardown", status);
    
    status = ((status == VIGICRUES_SUCCESS) && (memento == VIGICRUES_SUCCESS))
      ? VIGICRUES_SUCCESS : VIGICRUES_FAILURE;
  }
  
  return (status == VIGICRUES_SUCCESS)
    ? EXIT_SUCCESS : EXIT_FAILURE;
}

static inline void vigicrues_test_print_status(const char *step,
                                               vigicrues_status_t status)
{
  printf("%s : %s\n", step,
         (status == VIGICRUES_SUCCESS) ? "PASSED" : "FAILED");
}
