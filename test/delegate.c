/*
 * Copyright (c) 2017 Valentin Lahaye
 *
 * This file is part of a free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 3 of the License, or any later version.
 *
 * This program is distributed in the hope that it will be useful, but 
 * WITHOUT ANY WARRANTY; without even the implied warranty of 
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public License
 * along with this program. If not, see <http://www.gnu.org/licenses/lgpl-3.0.html>
 */

#include <stdlib.h>
#include <stdio.h>
#include <ctype.h>
#include <vigicrues/delegate.h>

vigicrues_status_t vigicrues_get_data(unsigned ent, const char *id,
                                      enum vigicrues_e type, unsigned timeout,
                                      vigicrues_data_t *p_data,
                                      vigicrues_error_t *p_error)
{
  vigicrues_status_t status = VIGICRUES_FAILURE;
  
  char *name = malloc(VIGICRUES_BUF_LENGTH);
  if (name) {
    snprintf(name, VIGICRUES_BUF_LENGTH,
             "%s.in-%c", id, tolower(type));
    
    FILE *f = fopen(name, "r");
    if (f) {
      fseek(f, 0L, SEEK_END);
      long size = ftell(f);
      rewind(f);
      
      p_data->buf = malloc(size + 1);
      if (p_data->buf) {
        size_t nmemb = fread(p_data->buf, size, 1, f);
        
        if (nmemb == 1) {
          p_data->buf[size] = '\0';
          p_data->size = size + 1;
          
          status = VIGICRUES_SUCCESS;
        } else {
          VIGICRUES_REPORT_ERROR(VIGICRUES_ERROR_OTHER);
        }
      } else {
        VIGICRUES_REPORT_ERROR(VIGICRUES_ERROR_OUT_OF_MEMORY);
      }
      
      fclose(f);
    } else {
      VIGICRUES_REPORT_ERROR(VIGICRUES_ERROR_INVALID);
    }
    
    free(name);
  } else {
    VIGICRUES_REPORT_ERROR(VIGICRUES_ERROR_OUT_OF_MEMORY);
  }
  
  return status;
}
