get_filename_component(CURRENT_PARENT_BINARY_DIR ${CMAKE_CURRENT_BINARY_DIR} PATH)

file(GLOB TESTS_IN *.in-[hqv])
file(COPY ${TESTS_IN} DESTINATION ${CURRENT_PARENT_BINARY_DIR})
