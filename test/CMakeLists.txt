add_library(vigicrues_test STATIC ${PROJECT_SOURCE_DIR}/src/vigicrues.c delegate.c)
set_target_properties(vigicrues_test PROPERTIES OUTPUT_NAME "vigicrues")
target_link_libraries(vigicrues_test ${MyHTML_LIBRARIES} ${JANSSON_LIBRARIES})

file(GLOB_RECURSE TESTS *.c)
list(REMOVE_ITEM TESTS "${CMAKE_CURRENT_SOURCE_DIR}/delegate.c")
list(REMOVE_ITEM TESTS "${CMAKE_CURRENT_SOURCE_DIR}/main.c")

foreach(TEST ${TESTS})
    file(RELATIVE_PATH TEST_NAME ${CMAKE_CURRENT_SOURCE_DIR} ${TEST})
    get_filename_component(TEST_EXT ${TEST_NAME} EXT)
    string(REPLACE "${TEST_EXT}" "" TEST_NAME "${TEST_NAME}")
    string(REGEX REPLACE /|"\\" "_" TEST_TARGET "${TEST_NAME}")
    string(CONCAT TEST_TARGET "test_" "${TEST_TARGET}")
    string(REGEX REPLACE /|"\\" "::" TEST_NAME "${TEST_NAME}")
    string(TOUPPER "${TEST_NAME}" TEST_NAME)

    add_executable(${TEST_TARGET} main.c ${TEST})
    target_link_libraries(${TEST_TARGET} vigicrues_test)
    add_test(NAME "${TEST_NAME}" COMMAND $<TARGET_FILE:${TEST_TARGET}>)
    set_tests_properties("${TEST_NAME}" PROPERTIES TIMEOUT 3)
endforeach()

add_subdirectory(in)
