/**
 * @file
 * @brief An example program which retreives the current vigilance state for
 * station Morlaix [Lannidy].
 * @author Valentin Lahaye
 * @copyright Copyright (c) 2017 Valentin Lahaye \n
 * This file is part of a free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 3 of the License, or any later version. \n
 * This program is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU
 * Lesser General Public License for more details. \n
 * You should have received a copy of the GNU Lesser General Public License
 * along with this program. If not, see <http://www.gnu.org/licenses/lgpl-3.0.html>
 * @see https://valentin.lahaye.pro/libvigicrues
 */

#include <stdlib.h>
#include <stdio.h>
#include <time.h>
#include <vigicrues.h>

#define VIGICRUES_LOIRE_BRETAGNE 8

static vigicrues_status_t func(const vigicrues_t *p_data, void *param)
{
  char *s = NULL;
  
  switch (p_data->vigilance) {
  case VIGICRUES_VIGILANCE_GREEN:
    s = "Green";
    break;    
  case VIGICRUES_VIGILANCE_YELLOW:
    s = "Yellow";
    break;
  case VIGICRUES_VIGILANCE_ORANGE:
    s = "Orange";
    break;
  case VIGICRUES_VIGILANCE_RED:
    s = "Red";
    break;
  }
  
  struct tm *p_date = localtime(&(p_data->timestamp));
  
  printf("Current vigilance state for station \"Morlaix [Lannidy]\":\n"
         "%s published on %s", s, asctime(p_date));
  
  return VIGICRUES_SUCCESS;
}

int main(void)
{
  vigicrues_status_t status = vigicrues_get(VIGICRUES_LOIRE_BRETAGNE,
                                            "J260301501", VIGICRUES_VIGILANCE,
                                            VIGICRUES_IGNORE_PARAM, &func, NULL,
                                            VIGICRUES_IGNORE_PARAM, NULL);
  
  return (status == VIGICRUES_SUCCESS) ? EXIT_SUCCESS : EXIT_FAILURE;
}
