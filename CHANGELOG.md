Changes
=======

Version 2.0.1
=============

Released 2018-01-03

* Bug fixes :

  - Fixed test failures on computers outside the CET timezone.
  - Fixed compilation failure when compiling with ``clang`` due to warning -Wmissing-field-initializers.

* Documentation :

  - Small documentation fixes.

Version 2.0
===========

Released 2017-08-05

* New features :

  - Added ``vigicrues_error_t`` as a replacement to ``errno`` for error
    reporting.

  - `vigicrues_get()` : Added a new ``vigicrues_error_t *`` parameter to return
    information to the caller about the error in case of failure. This parameter
    can be set to ``NULL`` to disable error reporting.

* Bug fixes :

  - Fixed several possible memory leaks in both tests and implementation.

* Tests :

  - Updated all tests according to the new error reporting mechanism and the new
    `vigicrues_get()` signature.

* Documentation :

  - Added documentation for ``vigicrues_error_t``.

  - Updated documentation for `vigicrues_get()`.

* Build :

  - Added the content of ``CURL_INCLUDE_DIRS``, ``JANSSON_INCLUDE_DIRS`` and
    ``MyHTML_INCLUDE_DIRS`` to be searched for header locations.

  - Added option ``VIGICRUES_BUILD_EXAMPLES`` to prevent building example
    applications. Use ``-DVIGICRUES_BUILD_EXAMPLES=Off`` to disable compiling
    examples.

  - Added option ``VIGICRUES_BUILD_TESTS`` to prevent building the test suite.
    Use ``-DVIGICRUES_BUILD_TESTS=Off`` to disable testing.

  - Added option ``VIGICRUES_BUILD_SHARED_LIBS`` to determine whether or not the
    library should be built dynamically or statically.
    Use ``-DVIGICRUES_BUILD_SHARED_LIBS=Off`` to build the library statically.

  - Added properties *VERSION* and *SOVERSION* to the library target when this
    one is built dynamically. When installing, appropriate symlinks are created
    if the platform supports symlinks.

Version 1.0.1
=============

Released 2017-07-28

* Documentation :

  - Small documentation fixes.

Version 1.0
===========

Released 2017-07-25

* Initial release
