/*
 * Copyright (c) 2017 Valentin Lahaye
 *
 * This file is part of a free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 3 of the License, or any later version.
 *
 * This program is distributed in the hope that it will be useful, but 
 * WITHOUT ANY WARRANTY; without even the implied warranty of 
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public License
 * along with this program. If not, see <http://www.gnu.org/licenses/lgpl-3.0.html>
 */

#include <stdlib.h>
#include <stdbool.h>
#include <string.h>
#include <ctype.h>
#include <jansson.h>
#include <myhtml/api.h>
#include <vigicrues.h>
#include <vigicrues/delegate.h>

static vigicrues_status_t vigicrues_parse_station_data(vigicrues_data_t *p_data, unsigned range,
                                                       vigicrues_f func, vigicrues_t *p_out, void *param,
                                                       vigicrues_error_t *p_error);

static vigicrues_status_t vigicrues_parse_section_data(vigicrues_data_t *p_data, unsigned ent, const char *id,
                                                       vigicrues_f func, vigicrues_t *p_out, void *param,
                                                       vigicrues_error_t *p_error);

vigicrues_status_t vigicrues_get(unsigned ent, const char *id,
                                 enum vigicrues_e type, unsigned range,
                                 vigicrues_f func, void *param,
                                 unsigned timeout, vigicrues_error_t *p_error)
{
  vigicrues_status_t status = VIGICRUES_SUCCESS;
  
  if (id && (strlen(id) == (sizeof("A000000000") - 1))) {
    if (isupper(id[0])) {
      int i = 0;
      while (isdigit(id[++i]));
      bool callable = (id[i] == '\0') && ((type == VIGICRUES_VIGILANCE)
        ? (ent > VIGICRUES_IGNORE_PARAM) && (range == VIGICRUES_IGNORE_PARAM)
        : (ent == VIGICRUES_IGNORE_PARAM) && (range > VIGICRUES_IGNORE_PARAM));
      if (!(callable)) {
        VIGICRUES_REPORT_ERROR(VIGICRUES_ERROR_INVALID);
        status = VIGICRUES_FAILURE;
      }
    }
  }
  
  if (status == VIGICRUES_SUCCESS) {
    vigicrues_data_t data = (vigicrues_data_t){ NULL, 0 };
    status = vigicrues_get_data(ent, id, type, timeout, &data, p_error);
    
    if (status == VIGICRUES_SUCCESS) {
      vigicrues_t out = (vigicrues_t){
        .type = type
      };
      
      status = (type == VIGICRUES_VIGILANCE)
        ? vigicrues_parse_section_data(&data, ent, id, func, &out, param,
                                       p_error)
        : vigicrues_parse_station_data(&data, range, func, &out, param,
                                       p_error);
      
      free(data.buf);
    }
  }
  
  return status;
}

#define VIGICRUES_SERVICE_VERSION "Beta 0.3"

static vigicrues_status_t vigicrues_parse_station_data(vigicrues_data_t *p_data, unsigned range,
                                                       vigicrues_f func, vigicrues_t *p_out, void *param,
                                                       vigicrues_error_t *p_error)
{
  vigicrues_status_t status = VIGICRUES_FAILURE;
  
  json_t *jdata = json_loads(p_data->buf, 0, NULL);
  if (jdata) {
    json_t *jcol = NULL;
    json_t *jobj = NULL;
    const char *version = NULL;
    const char *value = NULL;
    
    int code = json_unpack(jdata, "{s:s, s:o}",
                           "VersionFlux", &version, "Serie", &jobj);
    if (code == 0) {
      if ((json_typeof(jobj) == JSON_OBJECT) &&
          (strcmp(VIGICRUES_SERVICE_VERSION, version) == 0)) {
        jcol = json_object_get(jobj, "ObssHydro");
        
        if (jcol && (json_typeof(jcol) == JSON_ARRAY)) {
          status = VIGICRUES_SUCCESS;
          unsigned size = json_array_size(jcol);
          unsigned i;
          
          for (i = 0; (i < range) && (i < size) &&
                 (status == VIGICRUES_SUCCESS); i++) {
            // Values are retreived from most recent to oldest.
            json_t *jmember = json_array_get(jcol, size - i - 1);
            
            json_t *jdate = json_object_get(jmember, "DtObsHydro");
            json_t *jvalue = json_object_get(jmember, "ResObsHydro");
            
            if (jdate && jvalue) {
              value = json_string_value(jdate);
              if (value) {
                #ifdef __clang__
                #pragma clang diagnostic push
                #pragma clang diagnostic ignored "-Wmissing-field-initializers"
                #endif
                struct tm date = { 0 };
                #ifdef __clang__
                #pragma clang diagnostic pop
                #endif
                sscanf(value, "%d-%d-%dT%d:%d",
                       &(date.tm_year), &(date.tm_mon),
                       &(date.tm_mday), &(date.tm_hour),
                       &(date.tm_min));
                date.tm_year -= 1900;
                date.tm_mon -= 1;
                date.tm_isdst = -1;
                
                p_out->timestamp = mktime(&date);
                p_out->value = json_real_value(jvalue);
                
                status = func(p_out, param);
                
                if (status == VIGICRUES_FAILURE) {
                  VIGICRUES_REPORT_ERROR(VIGICRUES_ERROR_CANCELED);
                }
              } else {
                VIGICRUES_REPORT_ERROR(VIGICRUES_ERROR_OTHER);
                status = VIGICRUES_FAILURE;
              }
            } else {
              VIGICRUES_REPORT_ERROR(VIGICRUES_ERROR_OTHER);
              status = VIGICRUES_FAILURE;
            }
          }
          
          if ((i < range) && (status == VIGICRUES_SUCCESS)) {
            VIGICRUES_REPORT_ERROR(VIGICRUES_ERROR_RANGE);
            status = VIGICRUES_FAILURE;
          }
        } else {
          VIGICRUES_REPORT_ERROR(VIGICRUES_ERROR_OTHER);
        }
      } else {
        VIGICRUES_REPORT_ERROR(VIGICRUES_ERROR_OUTDATED);
      }
    } else {
      VIGICRUES_REPORT_ERROR(VIGICRUES_ERROR_INVALID);
    }
    
    json_decref(jdata);
  } else {
    VIGICRUES_REPORT_ERROR(VIGICRUES_ERROR_OUT_OF_MEMORY);
  }

  return status;
}

#undef VIGICRUES_SERVICE_VERSION
#define VIGICRUES_SERVICE_VERSION "1.7.0a"

static vigicrues_status_t vigicrues_parse_section_data(vigicrues_data_t *p_data, unsigned ent, const char *id,
                                                       vigicrues_f func, vigicrues_t *p_out, void *param,
                                                       vigicrues_error_t *p_error)
{
  vigicrues_status_t status = VIGICRUES_FAILURE;
  
  myhtml_t *myhtml = myhtml_create();
  myhtml_tree_t *mytree = myhtml_tree_create();
  
  if (myhtml && mytree) {
    mystatus_t mystatus = myhtml_init(myhtml, MyHTML_OPTIONS_DEFAULT, 1, 0);
    if (!(MyHTML_FAILED(mystatus))) {
      mystatus = myhtml_tree_init(mytree, myhtml);
      if (!(MyHTML_FAILED(mystatus))) {
        mystatus = myhtml_parse(mytree, MyENCODING_UTF_8, p_data->buf,
                                p_data->size);
      }
    }
    
    if (MyHTML_FAILED(mystatus)) {
      VIGICRUES_REPORT_ERROR((mystatus == MyHTML_STATUS_ERROR_MEMORY_ALLOCATION)
        ? VIGICRUES_ERROR_OUT_OF_MEMORY
        : VIGICRUES_ERROR_OTHER);
    } else {
      status = VIGICRUES_SUCCESS;
    }
  } else {
    VIGICRUES_REPORT_ERROR(VIGICRUES_ERROR_OUT_OF_MEMORY);
  }
  
  if (status == VIGICRUES_SUCCESS) {
    status = VIGICRUES_FAILURE;
    
    myhtml_collection_t *mycol = NULL;
    myhtml_tree_node_t *mynode = NULL;
    myhtml_tree_attr_t *myattr = NULL;
    const char *value = NULL;
    mystatus_t mystatus;
    
    mycol = myhtml_get_nodes_by_attribute_value(mytree, NULL, NULL, false,
                                                "name", sizeof("name") - 1,
                                                "description",
                                                sizeof("description") - 1,
                                                &mystatus);
    if (!(MyHTML_FAILED(mystatus))) {
      if (mycol->length > 0) {
        myattr = myhtml_attribute_by_key(mycol->list[0],
                                         "content",
                                         sizeof("content") - 1);
        if (myattr) {
          value = myhtml_attribute_value(myattr, NULL);
          if (value) {
            value = strstr(value, " : v");
            if (value && (strcmp(VIGICRUES_SERVICE_VERSION,
                                 value + sizeof(" : v") - 1) == 0)) {
              status = VIGICRUES_SUCCESS;
            }
          }
        }
        
        if (status == VIGICRUES_FAILURE) {
          VIGICRUES_REPORT_ERROR(VIGICRUES_ERROR_OUTDATED);
        }
      } else {
        VIGICRUES_REPORT_ERROR(VIGICRUES_ERROR_INVALID);
      }
      
      myhtml_collection_destroy(mycol);
    } else {
      VIGICRUES_REPORT_ERROR((mystatus == MyHTML_STATUS_ERROR_MEMORY_ALLOCATION)
        ? VIGICRUES_ERROR_OUT_OF_MEMORY
        : VIGICRUES_ERROR_OTHER);
    }
    
    if (status == VIGICRUES_SUCCESS) {
      status = VIGICRUES_FAILURE;
      
      char *url = (char *)malloc(VIGICRUES_BUF_LENGTH);
      if (url) {
        snprintf(url, VIGICRUES_BUF_LENGTH,
                 "niv3-station.php?CdEntVigiCru=%u&CdStationHydro=%s",
                 ent, id);
        
        mycol = myhtml_get_nodes_by_attribute_value(mytree, NULL,
                                                    NULL, false, "href",
                                                    sizeof("href") - 1,
                                                    url, strlen(url),
                                                    &mystatus);
        if (!(MyHTML_FAILED(mystatus))) {
          if (mycol->length > 0) {
            // li
            mynode = myhtml_node_parent(mycol->list[0]);
            // ul
            if (mynode) {
              mynode = myhtml_node_parent(mynode);
            }
            
            // a .basin-item-rss
            if (mynode) {
              mynode = myhtml_node_prev(mynode);
              if (mynode) {
                mynode = myhtml_node_prev(mynode);
              }
            }
            
            // .basin-item-vigilance
            if (mynode) {
              mynode = myhtml_node_prev(mynode);
            }
            
            if (mynode) {
              myattr = myhtml_attribute_by_key(mynode, "class",
                                               sizeof("class") - 1);
              if (myattr) {
                value = myhtml_attribute_value(myattr, NULL);
                
                if (value) {
                  if (strstr(value, " green")) {
                    p_out->vigilance = VIGICRUES_VIGILANCE_GREEN;
                  } else if (strstr(value, " yellow")) {
                    p_out->vigilance = VIGICRUES_VIGILANCE_YELLOW;
                  } else if (strstr(value, " orange")) {
                    p_out->vigilance = VIGICRUES_VIGILANCE_ORANGE;
                  } else if (strstr(value, " red")) {
                    p_out->vigilance = VIGICRUES_VIGILANCE_RED;
                  }
                  
                  status = VIGICRUES_SUCCESS;
                }
              }
            }
            
            if (status == VIGICRUES_FAILURE) {
              VIGICRUES_REPORT_ERROR(VIGICRUES_ERROR_OTHER);
            }
          } else {
            VIGICRUES_REPORT_ERROR(VIGICRUES_ERROR_INVALID);
          }
          
          myhtml_collection_destroy(mycol);
        } else {
          VIGICRUES_REPORT_ERROR((mystatus == MyHTML_STATUS_ERROR_MEMORY_ALLOCATION)
            ? VIGICRUES_ERROR_OUT_OF_MEMORY
            : VIGICRUES_ERROR_OTHER);
        }
        
        free(url);
      } else {
        VIGICRUES_REPORT_ERROR(VIGICRUES_ERROR_OUT_OF_MEMORY);
      }
    }
    
    if (status == VIGICRUES_SUCCESS) {
      status = VIGICRUES_FAILURE;
      
      mycol = myhtml_get_nodes_by_attribute_value(mytree, NULL, NULL,
                                                  false, "class",
                                                  sizeof("class") - 1,
                                                  "report-infos-def",
                                                  sizeof("report-infos-def") - 1,
                                                  &mystatus);
      if (!(MyHTML_FAILED(mystatus))) {
        if (mycol->length > 1) {
          mynode = myhtml_node_child(mycol->list[1]);
          if (mynode &&
              myhtml_node_tag_id(mynode) == MyHTML_TAG__TEXT) {
            value = myhtml_node_text(mynode, NULL);
            if (value) {
              #ifdef __clang__
              #pragma clang diagnostic push
              #pragma clang diagnostic ignored "-Wmissing-field-initializers"
              #endif
              struct tm date = { 0 };
              #ifdef __clang__
              #pragma clang diagnostic pop
              #endif
              sscanf(value, "%d.%d.%d à %d:%d",
                     &(date.tm_mday), &(date.tm_mon),
                     &(date.tm_year), &(date.tm_hour),
                     &(date.tm_min));
              date.tm_year -= 1900;
              date.tm_mon -= 1;
              date.tm_isdst = -1;
              
              p_out->timestamp = mktime(&date);
              
              status = func(p_out, param);
              if (status == VIGICRUES_FAILURE) {
                VIGICRUES_REPORT_ERROR(VIGICRUES_ERROR_CANCELED);
              }
            } else {
              VIGICRUES_REPORT_ERROR(VIGICRUES_ERROR_OTHER);
            }
          } else {
            VIGICRUES_REPORT_ERROR(VIGICRUES_ERROR_INVALID);
          }
        } else {
          VIGICRUES_REPORT_ERROR(VIGICRUES_ERROR_INVALID);
        }
        
        myhtml_collection_destroy(mycol);
      } else {
        VIGICRUES_REPORT_ERROR((mystatus == MyHTML_STATUS_ERROR_MEMORY_ALLOCATION)
          ? VIGICRUES_ERROR_OUT_OF_MEMORY
          : VIGICRUES_ERROR_OTHER);
      }
    }
  }
  
  if (mytree) {
    myhtml_tree_destroy(mytree);
  }
  
  if (myhtml) {
    myhtml_destroy(myhtml);
  }
  
  return status;
}
