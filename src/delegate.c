/*
 * Copyright (c) 2017 Valentin Lahaye
 *
 * This file is part of a free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 3 of the License, or any later version.
 *
 * This program is distributed in the hope that it will be useful, but 
 * WITHOUT ANY WARRANTY; without even the implied warranty of 
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public License
 * along with this program. If not, see <http://www.gnu.org/licenses/lgpl-3.0.html>
 */

#include <stdlib.h>
#include <stdio.h>
#include <string.h>
#include <errno.h>
#include <curl/curl.h>
#include <vigicrues/delegate.h>

#define VIGICRUES_HOST "https://www.vigicrues.gouv.fr"

static size_t vigicrues_bufferize_data(void *p_chunk, size_t size, size_t nmemb,
                                       void *p_data);

vigicrues_status_t vigicrues_get_data(unsigned ent, const char *id,
                                      enum vigicrues_e type, unsigned timeout,
                                      vigicrues_data_t *p_data,
                                      vigicrues_error_t *p_error)
{
  vigicrues_status_t status = VIGICRUES_FAILURE;
  
  char *url = (char *)malloc(VIGICRUES_BUF_LENGTH);
  if (url) {
    if (type == VIGICRUES_VIGILANCE) {
      snprintf(url, VIGICRUES_BUF_LENGTH,
               VIGICRUES_HOST "/niv2-bassin.php?CdEntVigiCru=%u", ent);
    } else {
      snprintf(url, VIGICRUES_BUF_LENGTH,
               VIGICRUES_HOST "/services/observations.json/?CdStationHydro=%s&GrdSerie=%c&FormatDate=iso",
               id, type);
    }
    
    CURL *curl = curl_easy_init();
    if (curl) {
      curl_easy_setopt(curl, CURLOPT_URL, url);
      if (timeout > VIGICRUES_IGNORE_PARAM) {
        curl_easy_setopt(curl, CURLOPT_TIMEOUT, timeout);
      }
      
      curl_easy_setopt(curl, CURLOPT_FOLLOWLOCATION, 1L);
      curl_easy_setopt(curl, CURLOPT_WRITEFUNCTION,
                       &vigicrues_bufferize_data);
      curl_easy_setopt(curl, CURLOPT_WRITEDATA, p_data);
      curl_easy_setopt(curl, CURLOPT_USERAGENT,
                       "libvigicrues/" VIGICRUES_VERSION);
      
      int code = curl_easy_perform(curl);
      switch (code) {
      case CURLE_OK:
        status = VIGICRUES_SUCCESS;
        break;
      case CURLE_OPERATION_TIMEDOUT:
        VIGICRUES_REPORT_ERROR(VIGICRUES_ERROR_TIMEDOUT);
        break;
      case CURLE_WRITE_ERROR:
      case CURLE_OUT_OF_MEMORY:
        VIGICRUES_REPORT_ERROR(VIGICRUES_ERROR_OUT_OF_MEMORY);
        break;
      default :
        VIGICRUES_REPORT_ERROR(VIGICRUES_ERROR_CANT_CONNECT);
        break;
      }
      
      curl_easy_cleanup(curl);
    } else {
      VIGICRUES_REPORT_ERROR(VIGICRUES_ERROR_OUT_OF_MEMORY);
    }
    
    free(url);
  } else {
    VIGICRUES_REPORT_ERROR(VIGICRUES_ERROR_OUT_OF_MEMORY);
  }
  
  return status;
}

static size_t vigicrues_bufferize_data(void *p_chunk, size_t size, size_t nmemb,
                                       void *param)
{
  vigicrues_data_t *p_data = (vigicrues_data_t *)param;
  size_t csize = size * nmemb;
  size = p_data->size + csize;
  
  p_data->buf = (char *)realloc(p_data->buf, size + 1);
  if (p_data->buf) {
    memcpy(p_data->buf + p_data->size, p_chunk, csize);
    p_data->buf[size] = '\0';
    p_data->size = size;
  } else {
    /* In case of an out of memory,
     * we decrement csize in order to return immediately from
     * the curl_easy_perform function with an error (CURLE_WRITE_ERROR). */
    --csize;
  }
  
  return csize;
}
