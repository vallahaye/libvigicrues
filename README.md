libvigicrues
============

[![Build status](https://travis-ci.org/vallahaye/libvigicrues.svg?branch=master)](https://travis-ci.org/vallahaye/libvigicrues) [![Documentation status](https://readthedocs.org/projects/libvigicrues/badge/?version=latest)](https://libvigicrues.readthedocs.io/en/latest)

**libvigicrues** is an API written in pure C99 to retrieve data from the vigicrues service \[[view](https://www.vigicrues.gouv.fr)].

The current version is `2.0.1`. [Last stable version](https://github.com/vallahaye/libvigicrues/releases/latest)

See [releases](https://github.com/vallahaye/libvigicrues/releases).

## Compilation and installation

### Dependencies

| Name    | Version | Link                                            |
| ------- | ------- | ----------------------------------------------- |
| myhtml  | `4`     | \[[view](https://github.com/lexborisov/myhtml)] |
| jansson | `2.8+`  | \[[view](http://www.digip.org/jansson)]         |

### Compilation

At the project's root, create a *build* directory :

    $ mkdir build && cd build
	
Then start compiling by running the following commands :

    $ cmake ..
    $ make

Use option ``VIGICRUES_BUILD_EXAMPLES`` to prevent building example applications :

    $ cmake -DVIGICRUES_BUILD_EXAMPLES=Off ..

Use option ``VIGICRUES_BUILD_TESTS`` to disable testing :

    $ cmake -DVIGICRUES_BUILD_TESTS=Off ..

Use option ``VIGICRUES_BUILD_SHARED_LIBS`` to build the library statically :

    $ cmake -DVIGICRUES_BUILD_SHARED_LIBS=Off ..

### Running the test suite

To run the test suite after compiling the project, call the *test* target by running the following command :

    $ make test

### Installation

To install the compiled library, run the following command :

    $ make install
    
### Documentation

Optionally, you can generate a complete documentation of the project by running the following command :

    $ make doc
	
This requires Doxygen \[[view](http://www.doxygen.org)] to be installed on your computer. The generated documentation is accessible following this path in a web browser : `file:///path/to/build/doc/html/index.html`

An online version of this documentation is also available [here](http://libvigicrues.readthedocs.io/en/latest/vigicrues_8h.html).

## Changes

Please, see [CHANGELOG.md](CHANGELOG.md).

## Authors

- Valentin Lahaye &lt;valentin@lahaye.pro&gt; ([website](https://lahaye.pro))

## Copyright

Copyright &copy; 2017 Valentin Lahaye.<br>
License LGPLv3+: GNU LGPL version 3 or later &lt;[https://www.gnu.org/licenses/lgpl.html](https://www.gnu.org/licenses/lgpl.html)&gt;<br>
This is a free software; see [sources](LICENSE) for copying conditions.
