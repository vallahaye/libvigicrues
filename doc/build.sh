#!/usr/bin/env sh

main ()
{
  SOURCES=`cd ../ && pwd`
  BUILD=$SOURCES/build
  NAME=libvigicrues
  VERSION=`sed -rn 's/^#define[ \t]+VIGICRUES_VERSION[ \t]+"([^"]+)".*/\1/p' $SOURCES/include/vigicrues.h`

  mkdir -p $BUILD/doc

  sed -e "s|@PROJECT_NAME@|$NAME|"          \
      -e "s|@PROJECT_VERSION@|$VERSION|"    \
      -e "s|@PROJECT_SOURCE_DIR@|$SOURCES|" \
      -e "s|@PROJECT_BINARY_DIR@|$BUILD|"   \
      Doxyfile.in > $BUILD/doc/Doxyfile

  doxygen $BUILD/doc/Doxyfile
}

main
