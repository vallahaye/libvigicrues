/*
 * Copyright (c) 2017 Valentin Lahaye
 *
 * This file is part of a free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 3 of the License, or any later version.
 *
 * This program is distributed in the hope that it will be useful, but 
 * WITHOUT ANY WARRANTY; without even the implied warranty of 
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public License
 * along with this program. If not, see <http://www.gnu.org/licenses/lgpl-3.0.html>
 */

#ifndef VIGICRUES_TEST_H
#define VIGICRUES_TEST_H

#include <vigicrues.h>

typedef struct {
  unsigned ent;
  const char *id;
  enum vigicrues_e type;
  unsigned range;
  void *param;
} vigicrues_test_t;

vigicrues_status_t vigicrues_test_setup(vigicrues_test_t *p_test);
vigicrues_status_t vigicrues_test_teardown(vigicrues_test_t *p_test);
vigicrues_status_t vigicrues_test_func(const vigicrues_t *p_data, void *param);
vigicrues_status_t vigicrues_test_validate(vigicrues_status_t status,
                                           vigicrues_error_t error);

#endif
