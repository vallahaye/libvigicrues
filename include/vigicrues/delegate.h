/*
 * Copyright (c) 2017 Valentin Lahaye
 *
 * This file is part of a free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 3 of the License, or any later version.
 *
 * This program is distributed in the hope that it will be useful, but 
 * WITHOUT ANY WARRANTY; without even the implied warranty of 
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public License
 * along with this program. If not, see <http://www.gnu.org/licenses/lgpl-3.0.html>
 */

#ifndef VIGICRUES_DELEGATE_H
#define VIGICRUES_DELEGATE_H

#include <vigicrues.h>

#define VIGICRUES_BUF_LENGTH 256

#define VIGICRUES_REPORT_ERROR(code)            \
  if (p_error) {                                \
    *p_error = (code);                          \
  }

typedef struct {
  char *buf;
  size_t size;
} vigicrues_data_t;

vigicrues_status_t vigicrues_get_data(unsigned ent, const char *id,
                                      enum vigicrues_e type, unsigned timeout,
                                      vigicrues_data_t *p_data,
                                      vigicrues_error_t *p_error);

#endif
