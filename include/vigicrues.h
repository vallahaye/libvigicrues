/**
 * @file
 * @brief File containing macros, types and function prototypes needed to work
 * with @b libvigicrues.
 * @author Valentin Lahaye
 * @copyright Copyright (c) 2017 Valentin Lahaye \n
 * This file is part of a free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 3 of the License, or any later version. \n
 * This program is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU
 * Lesser General Public License for more details. \n
 * You should have received a copy of the GNU Lesser General Public License
 * along with this program. If not, see <http://www.gnu.org/licenses/lgpl-3.0.html>
 * @see https://valentin.lahaye.pro/libvigicrues
 */

#ifndef VIGICRUES_H
#define VIGICRUES_H

#include <time.h>

/**
 * @hideinitializer
 * @brief An integer specifying the current major version.
 */
#define VIGICRUES_MAJOR_VERSION 2

/**
 * @hideinitializer
 * @brief An integer specifying the current minor version.
 */
#define VIGICRUES_MINOR_VERSION 0

/**
 * @hideinitializer
 * @brief An integer specifying the current patch version.
 */
#define VIGICRUES_PATCH_VERSION 1

/**
 * @hideinitializer
 * @brief A string representation of the current version.
 * @details Patch version is ommited if it's 0.
 */
#define VIGICRUES_VERSION "2.0.1"

/**
 * @hideinitializer
 * @brief A 3-byte hexadecimal representation of the current version, e.g. 
 * @b 0x010203 for version @b 1.2.3.
 * @details This is useful in numeric comparisons, e.g. :
 * @code
   #if VIGICRUES_VERSION_HEX >= 0x010203
   // Code specific to version 1.2.3 and above
   #endif
 * @endcode
 */
#define VIGICRUES_VERSION_HEX ((VIGICRUES_MAJOR_VERSION << 16) |  \
                               (VIGICRUES_MINOR_VERSION << 8)  |  \
                               (VIGICRUES_PATCH_VERSION << 0))

/**
 * @hideinitializer
 * @brief An integer used to bypass the @p ent, @p range and @p timeout
 * parameters when calling the function vigicrues_get().
 * @see vigicrues_get
 */
#define VIGICRUES_IGNORE_PARAM 0

#ifdef __cplusplus
extern "C" {
#endif

/**
 * @brief Specifies the type of data to retreive for a station.
 */
enum vigicrues_e {
  VIGICRUES_HEIGHT = 'H', /**< Retreive the water heights. @hideinitializer */
  VIGICRUES_FLOW   = 'Q', /**< Retreive the water flows. @hideinitializer */
  VIGICRUES_VIGILANCE = 'V' /**< Retreive the current vigilance state. @hideinitializer */
};

/**
 * @brief Specifies the different vigilance states.
 */
enum vigicrues_vigilance_e {
  VIGICRUES_VIGILANCE_GREEN, /**< Green vigilance state. */
  VIGICRUES_VIGILANCE_YELLOW, /**< Yellow vigilance state. */
  VIGICRUES_VIGILANCE_ORANGE, /**< Orange vigilance state. */
  VIGICRUES_VIGILANCE_RED /**< Red vigilance state. */
};

/**
 * @brief A structure to hold the data retreived from the vigicrues service.
 */
typedef struct {
  enum vigicrues_e type; /**< The type of data retreived. */
  float value; /**< The retreived value (either a height or a flow). This field
                  is left unspecified if @p type is equal to
                  @b VIGICRUES_VIGILANCE. */
  enum vigicrues_vigilance_e vigilance; /**< The vigilance state retreived. This
                                           field is left unspecified if @p type
                                           is equal to either @b VIGICRUES_HEIGHT
                                           or @b VIGICRUES_FLOW. */
  time_t timestamp; /**< The date (expressed in the UNIX epoch time format) at
                       which the retreived data was published on the vigicrues
                       service. */
} vigicrues_t;

/**
 * @brief Specifies the different error codes that can be reported in case of
 * failure.
 * @details The normal use of @b vigicrues_error_t is to allocate it on the
 * stack, and pass a pointer to a function, e.g. :
 * @code
   int main(void) {
     vigicrues_error_t error;
     vigicrues_status_t status = vigicrues_get(VIGICRUES_IGNORE_PARAM, "J262301002",
                                               VIGICRUES_HEIGHT, 1, &func, NULL,
                                               VIGICRUES_IGNORE_PARAM, &error);
     if (status == VIGICRUES_FAILURE) {
         // The error variable contains error informations
     }
     ...
   }
 * @endcode
 * Also note that if the call succeeded, the content of @p p_error is left
 * unspecified. All functions also accept @b NULL as the @b vigicrues_error_t
 * pointer, in which case no error information is returned to the caller.
 */
typedef enum {
  VIGICRUES_ERROR_INVALID = 1, /**< One parameter or more is incorrect. @hideinitializer */
  VIGICRUES_ERROR_RANGE, /**< All requested values could not be returned to the caller. */
  VIGICRUES_ERROR_OUTDATED, /**< The vigicrues service version has changed, the library needs to be updated. */
  VIGICRUES_ERROR_CANT_CONNECT, /**< Can't connect to the vigicrues service. */
  VIGICRUES_ERROR_TIMEDOUT, /**< The maximum time allowed for the transfer operation to take has been exceeded. */
  VIGICRUES_ERROR_CANCELED, /**< The function has been stopped by the user defined routine. */
  VIGICRUES_ERROR_OUT_OF_MEMORY, /**< A memory allocation request failed. */
  VIGICRUES_ERROR_OTHER /**< An unhandled error occurred. */
} vigicrues_error_t;

/**
 * @brief Specifies the values that can be returned by a function to indicate
 * its execution status.
 */
typedef enum {
  VIGICRUES_FAILURE = -1, /**< Unsuccessful execution of a function. @hideinitializer */
  VIGICRUES_SUCCESS =  0 /**< Successful execution of a function. @hideinitializer */
} vigicrues_status_t;

/**
 * @brief Specifies the signature each user defined callback must respect.
 * @param[in] p_data A pointer to the data retreived by the function
 * vigicrues_get(). This pointer makes reference to a statically allocated
 * structure and therefore must not be freed.
 * @param[in] param A user defined parameter passed to the callback each time
 * it's being fired.
 * @retval VIGICRUES_FAILURE Unsuccessful execution of the callback. The caller
 * routine e.g. vigicrues_get() is stopped and returns immediately with status
 * @b VIGICRUES_FAILURE.
 * @retval VIGICRUES_SUCCESS Successful execution of the callback.
 * @see vigicrues_get
 */
typedef vigicrues_status_t (*vigicrues_f)(const vigicrues_t *p_data, void *param);

/**
 * @brief Retreives data from the vigicrues service.
 * @details In case @p range is greater than 1, values are retreived from most
 * recent to oldest.
 * @param[in] ent The ID of the flood forecasting service to which the station
 * belongs. This parameter must be set to @b VIGICRUES_IGNORE_PARAM if
 * @p type is equal to either @b VIGICRUES_HEIGHT or @b VIGICRUES_FLOW.
 * @param[in] id The ID of the station for which to retreive data.
 * @param[in] type The type of data to retreive.
 * @param[in] range The number of values to retreive. This parameter must be set
 * to @b VIGICRUES_IGNORE_PARAM if @p type is equal to @b VIGICRUES_VIGILANCE.
 * @param[in] func A user defined function to be called each time a value is
 * retreived.
 * @param[in] param A user defined parameter to be passed to the callback each
 * time it's being fired. This parameter is optional and therefore can be set to
 * @b NULL.
 * @param[in] timeout The maximum time in seconds that you allow the function
 * transfer operation to take. Set this parameter to @b VIGICRUES_IGNORE_PARAM
 * for it to never time out.
 * @param[out] p_error A pointer to a @b vigicrues_error_t variable to store
 * error information in case of failure. This parameter can be set to @b NULL to
 * ignore error reporting.
 * @retval VIGICRUES_FAILURE Unsuccessful execution of the function, in which
 * case @p p_error is filled with information about the error.
 * @retval VIGICRUES_SUCCESS Successful execution of the function.
 * @see VIGICRUES_IGNORE_PARAM
 */
vigicrues_status_t vigicrues_get(unsigned ent, const char *id,
                                 enum vigicrues_e type, unsigned range,
                                 vigicrues_f func, void *param,
                                 unsigned timeout, vigicrues_error_t *p_error);

/**
 * @example vigilance.c
 * A detailed example on how to retreive and handle the current vigilance state
 * of a station (<a href="https://www.vigicrues.gouv.fr/niv2-bassin.php?CdEntVigiCru=8">Morlaix [Lannidy]</a>
 * in this example).
 */

/**
 * @example heights.c
 * A detailed example on how to retreive and handle the 10 last water heights of
 * a station
 * (<a href="https://www.vigicrues.gouv.fr/niv3-station.php?CdStationHydro=J260301501&CdEntVigiCru=8">
 * Morlaix [Lannidy]</a> in this example).
 */

#ifdef __cplusplus
}
#endif

#endif
