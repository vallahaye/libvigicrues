# Find libvigicrues
# The modules defines the following variables :
#
#   VIGICRUES_FOUND        - true if libvigicrues is found
#   VIGICRUES_VERSION      - the version of libvigicrues
#   VIGICRUES_INCLUDE_DIRS - the path to the libvigicrues headers
#   VIGICRUES_LIBRARIES    - the libvigicrues libraries
#
# Copyright (c) 2017 Valentin Lahaye
#
# This file is part of a free software; you can redistribute it and/or
# modify it under the terms of the GNU Lesser General Public
# License as published by the Free Software Foundation; either
# version 3 of the License, or any later version.
#
# This program is distributed in the hope that it will be useful, but 
# WITHOUT ANY WARRANTY; without even the implied warranty of 
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU
# Lesser General Public License for more details.
#
# You should have received a copy of the GNU Lesser General Public License along
# with this program. If not, see <http://www.gnu.org/licenses/lgpl-3.0.html>

find_package(PkgConfig QUIET)
pkg_check_modules(PC_VIGICRUES QUIET vigicrues)

find_path(VIGICRUES_INCLUDE_DIR
    NAMES vigicrues.h
    HINTS ${PC_VIGICRUES_INCLUDE_DIRS})

find_library(VIGICRUES_LIBRARY
    NAMES vigicrues
    HINTS ${PC_VIGICRUES_LIBRARY_DIRS})

if(VIGICRUES_INCLUDE_DIR)
     set(VERSION_REGEX "^#define[ \t]+VIGICRUES_VERSION[ \t]+\"([^\"]+)\".*")
     file(STRINGS "${VIGICRUES_INCLUDE_DIR}/vigicrues.h" VIGICRUES_VERSION
          REGEX "${VERSION_REGEX}")
     string(REGEX REPLACE "${VERSION_REGEX}" "\\1"
            VIGICRUES_VERSION "${VIGICRUES_VERSION}")
     unset(VERSION_REGEX)
endif()

include(FindPackageHandleStandardArgs)

find_package_handle_standard_args(Vigicrues REQUIRED_VARS
                                  VIGICRUES_INCLUDE_DIR VIGICRUES_LIBRARY
                                  VERSION_VAR VIGICRUES_VERSION)
                                  
if(VIGICRUES_FOUND)
     set(VIGICRUES_INCLUDE_DIRS ${VIGICRUES_INCLUDE_DIR})
     set(VIGICRUES_LIBRARIES ${VIGICRUES_LIBRARY})
endif()

mark_as_advanced(VIGICRUES_INCLUDE_DIR VIGICRUES_LIBRARY)
