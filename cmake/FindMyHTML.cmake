# Find MyHTML
# The modules defines the following variables :
#
#   MyHTML_FOUND        - true if MyHTML is found
#   MyHTML_VERSION      - the version of MyHTML
#   MyHTML_INCLUDE_DIRS - the path to the MyHTML headers
#   MyHTML_LIBRARIES    - the MyHTML libraries
#
# Copyright (c) 2017 Valentin Lahaye
#
# This file is part of a free software; you can redistribute it and/or
# modify it under the terms of the GNU Lesser General Public
# License as published by the Free Software Foundation; either
# version 3 of the License, or any later version.
#
# This program is distributed in the hope that it will be useful, but 
# WITHOUT ANY WARRANTY; without even the implied warranty of 
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU
# Lesser General Public License for more details.
#
# You should have received a copy of the GNU Lesser General Public License along
# with this program. If not, see <http://www.gnu.org/licenses/lgpl-3.0.html>

find_package(PkgConfig QUIET)
pkg_check_modules(PC_MyHTML QUIET myhtml)

find_path(MyHTML_INCLUDE_DIR
    NAMES myhtml/api.h
    HINTS ${PC_MyHTML_INCLUDE_DIRS})

find_library(MyHTML_LIBRARY
    NAMES myhtml
    HINTS ${PC_MyHTML_LIBRARY_DIRS})

if(MyHTML_INCLUDE_DIR)
     set(MAJOR_VERSION_REGEX "#define[ \t]+MyHTML_VERSION_MAJOR[ \t]+([0-9]+).*")
     set(MINOR_VERSION_REGEX "#define[ \t]+MyHTML_VERSION_MINOR[ \t]+([0-9]+).*")
     set(PATCH_VERSION_REGEX "#define[ \t]+MyHTML_VERSION_PATCH[ \t]+([0-9]+).*")
     file(READ "${MyHTML_INCLUDE_DIR}/myhtml/api.h" MyHTML_HEADER_CONTENT)
     string(REGEX MATCH "${MAJOR_VERSION_REGEX}"
            MyHTML_MAJOR_VERSION "${MyHTML_HEADER_CONTENT}")
     set(MyHTML_MAJOR_VERSION "${CMAKE_MATCH_1}")
     string(REGEX MATCH "${MINOR_VERSION_REGEX}"
            MyHTML_MINOR_VERSION "${MyHTML_HEADER_CONTENT}")
     set(MyHTML_MINOR_VERSION "${CMAKE_MATCH_1}")
     string(REGEX MATCH "${PATCH_VERSION_REGEX}"
            MyHTML_PATCH_VERSION "${MyHTML_HEADER_CONTENT}")
     set(MyHTML_PATCH_VERSION "${CMAKE_MATCH_1}")
     set(MyHTML_VERSION "${MyHTML_MAJOR_VERSION}.${MyHTML_MINOR_VERSION}.${MyHTML_PATCH_VERSION}")
     unset(MyHTML_MAJOR_VERSION)
     unset(MyHTML_MINOR_VERSION)
     unset(MyHTML_PATCH_VERSION)
     unset(MyHTML_HEADER_CONTENT)
     unset(MAJOR_VERSION_REGEX)
     unset(MINOR_VERSION_REGEX)
     unset(PATCH_VERSION_REGEX)
endif()

include(FindPackageHandleStandardArgs)

find_package_handle_standard_args(MyHTML REQUIRED_VARS
                                  MyHTML_INCLUDE_DIR MyHTML_LIBRARY
                                  VERSION_VAR MyHTML_VERSION)
                                  
if(MyHTML_FOUND)
     set(MyHTML_INCLUDE_DIRS ${MyHTML_INCLUDE_DIR})
     set(MyHTML_LIBRARIES ${MyHTML_LIBRARY})
endif()

mark_as_advanced(MyHTML_INCLUDE_DIR MyHTML_LIBRARY)
