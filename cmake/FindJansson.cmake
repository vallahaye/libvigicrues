# Find Jansson
# The modules defines the following variables :
#
#   JANSSON_FOUND        - true if Jansson is found
#   JANSSON_VERSION      - the version of Jansson
#   JANSSON_INCLUDE_DIRS - the path to the Jansson headers
#   JANSSON_LIBRARIES    - the Jansson libraries
#
# Copyright (c) 2017 Valentin Lahaye
#
# This file is part of a free software; you can redistribute it and/or
# modify it under the terms of the GNU Lesser General Public
# License as published by the Free Software Foundation; either
# version 3 of the License, or any later version.
#
# This program is distributed in the hope that it will be useful, but 
# WITHOUT ANY WARRANTY; without even the implied warranty of 
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU
# Lesser General Public License for more details.
#
# You should have received a copy of the GNU Lesser General Public License along
# with this program. If not, see <http://www.gnu.org/licenses/lgpl-3.0.html>

find_package(PkgConfig QUIET)
pkg_check_modules(PC_JANSSON QUIET jansson)

find_path(JANSSON_INCLUDE_DIR
    NAMES jansson.h
    HINTS ${PC_JANSSON_INCLUDE_DIRS})

find_library(JANSSON_LIBRARY
    NAMES jansson
    HINTS ${PC_JANSSON_LIBRARY_DIRS})

if(JANSSON_INCLUDE_DIR)
     set(VERSION_REGEX "^#define[ \t]+JANSSON_VERSION[ \t]+\"([^\"]+)\".*")
     file(STRINGS "${JANSSON_INCLUDE_DIR}/jansson.h" JANSSON_VERSION
          REGEX "${VERSION_REGEX}")
     string(REGEX REPLACE "${VERSION_REGEX}" "\\1"
            JANSSON_VERSION "${JANSSON_VERSION}")
     unset(VERSION_REGEX)
endif()

include(FindPackageHandleStandardArgs)

find_package_handle_standard_args(Jansson REQUIRED_VARS
                                  JANSSON_INCLUDE_DIR JANSSON_LIBRARY
                                  VERSION_VAR JANSSON_VERSION)
                                  
if(JANSSON_FOUND)
     set(JANSSON_INCLUDE_DIRS ${JANSSON_INCLUDE_DIR})
     set(JANSSON_LIBRARIES ${JANSSON_LIBRARY})
endif()

mark_as_advanced(JANSSON_INCLUDE_DIR JANSSON_LIBRARY)
